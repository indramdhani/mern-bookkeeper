const mongoose = require('mongoose');

const authorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  date_created: {
    type: Date
  },
  date_updated: {
    type: Date,
    default: Date.now
  },
  date_deleted: {
    type: Date,
    default: null
  }
});

module.exports = Author = mongoose.model('author', authorSchema);
