const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'author'
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'category'
  },
  publisher: { type: mongoose.Schema.Types.ObjectId, ref: 'publisher' },
  date_created: {
    type: Date
  },
  date_updated: {
    type: Date,
    default: Date.now
  },
  date_deleted: {
    type: Date,
    default: null
  }
});

module.exports = Book = mongoose.model('book', bookSchema);
