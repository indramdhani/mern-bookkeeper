const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  date_created: {
    type: Date
  },
  date_updated: {
    type: Date,
    default: Date.now
  },
  date_deleted: {
    type: Date,
    default: null
  }
});

module.exports = Category = mongoose.model('category', categorySchema);
