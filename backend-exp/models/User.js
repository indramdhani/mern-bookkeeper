const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  date_created: {
    type: Date
  },
  date_updated: {
    type: Date,
    default: Date.now
  },
  date_deleted: {
    type: Date,
    default: null
  }
});

module.exports = User = mongoose.model('user', userSchema);
