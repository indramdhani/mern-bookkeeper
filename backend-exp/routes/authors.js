const express = require('express');
const { check, validationResult } = require('express-validator/check');
const router = express.Router();
const auth = require('../middleware/auth');
const Author = require('../models/Author');

/* 
@route  /authors
@desc   get all authors  
*/
router.get('/', auth, async (req, res, next) => {
  try {
    const authors = await Author.find();
    return res.status(200).json({ data: { data: authors } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /authors/:id
@desc   get author based on id  
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const author = await Author.findById(req.params.id);
    if (!author) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: author } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /authors/user/:id
@desc   get author based on id  
*/
router.get('/user/:id', auth, async (req, res) => {
  try {
    const authors = await Author.find({ user: req.params.id });
    if (!authors) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: authors } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /authors/add
@desc   add the author  
*/
router.post(
  '/add',
  [
    auth,
    check('name', 'Name is required.')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;

    try {
      let author = await Author.find({ name: name, user: req.user.id });
      if (author.length > 0) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data already exist.' }] });
      }
      const date_created = Date.now();
      const user = req.user.id;
      author = new Author({
        name,
        description,
        user,
        date_created
      });
      await author.save();
      return res.status(200).json({ data: { data: author } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/* 
@route  /authors/update/:id
@desc   Update the author  
*/
router.post(
  '/update/:id',
  [
    auth,
    check('name', 'Name is required.')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;
    const dataToUpdate = {};
    if (name) dataToUpdate.name = name;
    if (description) dataToUpdate.description = description;
    dataToUpdate.date_updated = Date.now();
    try {
      let author = await Author.findById(req.params.id);
      if (!author) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not exist.' }] });
      }
      author = await Author.findOneAndUpdate(
        { _id: author._id },
        { $set: dataToUpdate },
        { new: true }
      );
      return res.status(200).json({ data: { data: author } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/* 
@route  /authors/:id
@desc   delete author based on id  
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    // remove author
    await Author.findOneAndRemove({ _id: req.params.id });
    return res.status(200).json({ data: { msg: 'Data Deleted' } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

module.exports = router;
