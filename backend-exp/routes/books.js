const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../middleware/auth');
const Book = require('../models/Book');
/* 
@route  /books
@desc   get all books  
*/
router.get('/', auth, async (req, res, next) => {
  try {
    const books = await Book.find()
      .populate('author', 'name')
      .populate('category', 'name')
      .populate('publisher', 'name');
    return res.status(200).json({ data: { data: books } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route  /books/:id
@desc get book by id
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const book = await Book.findById(req.params.id)
      .populate('author', 'name')
      .populate('category', 'name')
      .populate('publisher', 'name');
    if (!book) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: book } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /books/user/:id
@desc   get book based on id  
*/
router.get('/user/:id', auth, async (req, res) => {
  try {
    const books = await Book.find({ user: req.params.id })
      .populate('author', 'name')
      .populate('category', 'name')
      .populate('publisher', 'name');
    if (!books) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: books } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route /categories/add
@desc Add the category
*/
router.post(
  '/add',
  [
    auth,
    check('name', 'Name is required.')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description, author, category, publisher } = req.body;

    try {
      let book = await Book.find({ name: name, user: req.user.id });
      if (book.length > 0) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data already exist.' }] });
      }
      const user = req.user.id;
      const date_created = Date.now();
      book = new Book({
        name,
        description,
        user,
        author,
        category,
        publisher,
        date_created
      });
      await book.save();
      return res.status(200).json({ data: { data: book } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /books/update/:id
@desc Update the book
*/
router.post(
  '/update/:id',
  [auth, check('name', 'Name is required.')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description, author, category, publisher } = req.body;
    const dataToUpdate = {};
    if (name) dataToUpdate.name = name;
    if (description) dataToUpdate.description = description;
    if (author) dataToUpdate.author = author;
    if (category) dataToUpdate.category = category;
    if (publisher) dataToUpdate.publisher = publisher;
    dataToUpdate.date_updated = Date.now();
    try {
      let book = await Book.findById(req.params.id);
      if (!book) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not exist.' }] });
      }
      book = await Book.findOneAndUpdate(
        { _id: book._id },
        { $set: dataToUpdate },
        { new: true }
      );
      return res.status(200).json({ data: { data: book } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /books/:id
@desc delete the book based on id
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    await Book.findOneAndRemove({ _id: req.params.id });
    return res.status(200).json({ data: { msg: 'Data deleted.' } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

module.exports = router;
