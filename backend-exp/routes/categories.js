const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../middleware/auth');
const Category = require('../models/Category');
/* 
@route  /categories
@desc   get all categories  
*/
router.get('/', auth, async (req, res, next) => {
  try {
    const categories = await Category.find();
    return res.status(200).json({ data: { data: categories } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route  /categories/:id
@desc get category by id
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    if (!category) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: category } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /categories/user/:id
@desc   get category based on id  
*/
router.get('/user/:id', auth, async (req, res) => {
  try {
    const categories = await Category.find({ user: req.params.id });
    if (!categories) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: categories } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route /categories/add
@desc Add the category
*/
router.post(
  '/add',
  [
    auth,
    check('name', 'Name is required.')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;

    try {
      let category = await Category.find({ name: name, user: req.user.id });
      if (category.length > 0) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data already exist.' }] });
      }
      const user = req.user.id;
      const date_created = Date.now();
      category = new Category({
        name,
        description,
        user,
        date_created
      });
      await category.save();
      return res.status(200).json({ data: { data: category } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /categories/update/:id
@desc Update the category
*/
router.post(
  '/update/:id',
  [auth, check('name', 'Name is required.')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;
    const dataToUpdate = {};
    if (name) dataToUpdate.name = name;
    if (description) dataToUpdate.description = description;
    dataToUpdate.date_updated = Date.now();
    try {
      let category = await Category.findById(req.params.id);
      if (!category) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not exist.' }] });
      }
      category = await Category.findOneAndUpdate(
        { _id: category._id },
        { $set: dataToUpdate },
        { new: true }
      );
      return res.status(200).json({ data: { data: category } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /categories/:id
@desc delete the category based on id
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    await Category.findOneAndRemove({ _id: req.params.id });
    return res.status(200).json({ data: { msg: 'Data deleted.' } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

module.exports = router;
