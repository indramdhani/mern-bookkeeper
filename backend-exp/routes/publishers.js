const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../middleware/auth');
const Publisher = require('../models/Publisher');

/* 
@route  /publisher
@desc   get all publisher  
*/
router.get('/', auth, async (req, res, next) => {
  try {
    const publishers = await Publisher.find();
    return res.status(200).json({ data: { data: publishers } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route  /publishers/:id
@desc get publisher by id
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const publisher = await Publisher.findById(req.params.id);
    if (!publisher) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: publisher } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /publishers/user/:id
@desc   get publisher based on id  
*/
router.get('/user/:id', auth, async (req, res) => {
  try {
    const publishers = await Publisher.find({ user: req.params.id });
    if (!publishers) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: publishers } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/*
@route /publishers/add
@desc Add the publisher
*/
router.post(
  '/add',
  [
    auth,
    check('name', 'Name is required.')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;
    try {
      let publisher = await Publisher.find({ name: name, user: req.user.id });
      if (publisher.length > 0) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data already exist.' }] });
      }
      const user = req.user.id;
      const date_created = Date.now();
      publisher = new Publisher({
        name,
        description,
        user,
        date_created
      });
      await publisher.save();
      return res.status(200).json({ data: { data: publisher } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /publishers/update/:id
@desc Update the publisher
*/
router.post(
  '/update/:id',
  [auth, check('name', 'Name is required.')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, description } = req.body;
    const dataToUpdate = {};
    if (name) dataToUpdate.name = name;
    if (description) dataToUpdate.description = description;
    dataToUpdate.date_updated = Date.now();
    try {
      let publisher = await Publisher.findById(req.params.id);
      if (!publisher) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not exist.' }] });
      }
      publisher = await Publisher.findOneAndUpdate(
        { _id: publisher._id },
        { $set: dataToUpdate },
        { new: true }
      );
      return res.status(200).json({ data: { data: publisher } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/*
@route /publishers/:id
@desc delete the publisher based on id
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    await Publisher.findOneAndRemove({ _id: req.params.id });
    return res.status(200).json({ data: { msg: 'Data deleted.' } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

module.exports = router;
