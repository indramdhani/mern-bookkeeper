const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const { check, validationResult } = require('express-validator/check');
const bcrypt = require('bcrypt');
const User = require('../models/User');

/* 
@route  /users
@desc   get all users  
*/
router.get('/', auth, async (req, res, next) => {
  try {
    const users = await User.find().select('-password');
    return res.status(200).json({ data: { data: users } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /users/me
@desc   get logged in user  
*/
router.get('/me', auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password');
    if (!user) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: user } });
  } catch (error) {
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /users/:id
@desc   get user based on id  
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const user = await User.findById(req.params.id).select('-password');
    if (!user) {
      return res.status(400).json({ errors: [{ msg: 'Data is not exist.' }] });
    }
    return res.status(200).json({ data: { data: user } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

/* 
@route  /users/update/:id
@desc   Update the user  
*/
router.post(
  '/update/:id',
  [auth, check('email', 'Email is required.').isEmail()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, email, password } = req.body;
    const dataToUpdate = {};
    if (name) dataToUpdate.name = name;
    if (email) dataToUpdate.email = email;
    if (password) {
      const salt = await bcrypt.genSalt(10);
      dataToUpdate.password = await bcrypt.hash(password, salt);
    }
    dataToUpdate.date_updated = Date.now();
    try {
      let user = await User.findById(req.params.id);
      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not exist.' }] });
      }
      user = await User.findOneAndUpdate(
        { _id: user._id },
        { $set: dataToUpdate },
        { new: true }
      );
      return res.status(200).json({ data: { data: user } });
    } catch (error) {
      if (error.kind == 'ObjectId') {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Data is not found.' }] });
      }
      return res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
);

/* 
@route  /users/:id
@desc   delete user based on id  
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    //   @TODO - remove all users data
    // remove user
    await User.findOneAndRemove({ _id: req.params.id });
    return res.status(200).json({ data: { msg: 'Data Deleted' } });
  } catch (error) {
    if (error.kind == 'ObjectId') {
      return res.status(400).json({ errors: [{ msg: 'Data is not found.' }] });
    }
    return res.status(500).json({ errors: [{ msg: error.message }] });
  }
});

module.exports = router;
