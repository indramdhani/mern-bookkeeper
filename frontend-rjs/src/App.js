import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { loadUser } from './actions/auth';
import setAuthToken from './utils/setAuthToken';
import './App.css';

import Navbar from './components/layout/Navbar';
import Alert from './components/layout/Alert';
import Landing from './components/landing/Landing';
// import Dashboard from './components/dashboard/Dashboard';

import { Provider } from 'react-redux';
import store from './store';

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <section className='container mt-4'>
            <Alert />
            <Route exact path='/' component={Landing} />
            <Switch>
              {/* <PrivateRoute exact path='/dashboard' component={Dashboard} /> */}
            </Switch>
          </section>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;
