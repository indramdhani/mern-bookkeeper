import React, { Fragment, useState } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../actions/auth';
import { setAlert } from '../../actions/alert';
import config from '../../config';

const Landing = ({ setAlert, login, auth: { isAuthenticated, loading } }) => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
    submit: ''
  });

  const { email, password, submit } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    let password_hash = await crypto
      .createHash('md5')
      .update('password')
      .digest('hex');
    if (submit === 'Register') {
      console.log(formData);
    } else {
      login({ email, password_hash });
    }
  };

  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  return (
    <Fragment>
      <div className='jumbotron bg-white border border-secondary'>
        <h1 className='display-4'>{config.TITLE}</h1>
        <p className='lead text-muted'>
          Gunakan aplikasi ini untuk mencatat daftar buku yang kamu miliki.
        </p>
        <hr className='my-4' />
        <form name='user' onSubmit={e => onSubmit(e)}>
          <div className='form-group'>
            <label htmlFor='email'>Email address</label>
            <input
              type='email'
              name='email'
              className='form-control'
              id='email'
              aria-describedby='emailHelp'
              placeholder='Enter email'
              value={email}
              onChange={e => onChange(e)}
            />
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              name='password'
              className='form-control'
              id='password'
              placeholder='Enter password'
              value={password}
              onChange={e => onChange(e)}
            />
          </div>
          <input
            type='submit'
            name='submit'
            className='btn btn-block btn-light'
            value='Login'
            onClick={e => onChange(e)}
          />
          <input
            type='submit'
            name='submit'
            className='btn btn-block btn-dark'
            value='Register'
            onClick={e => onChange(e)}
          />
        </form>
      </div>
    </Fragment>
  );
};

Landing.propTypes = {
  setAlert: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { setAlert, login }
)(Landing);
