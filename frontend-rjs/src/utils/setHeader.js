import axios from 'axios';

const setHeader = () => {
  axios.defaults.headers.common['Content-Type'] = 'application/json';
};

export default setHeader;
